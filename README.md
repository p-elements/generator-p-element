# generator-p-element [![NPM version][npm-image]][npm-url] [![Build Status][travis-image]][travis-url] [![Dependency Status][daviddm-image]][daviddm-url]
> Generator for p-element

## Installation

First, install [Yeoman](http://yeoman.io) and generator-p-element using [npm](https://www.npmjs.com/) (we assume you have pre-installed [node.js](https://nodejs.org/)).

```bash
npm install -g yo
npm install -g generator-p-element
```

Then generate your new project:

```bash
yo p-element
```

## Getting To Know Yeoman

 * Yeoman has a heart of gold.
 * Yeoman is a person with feelings and opinions, but is very easy to work with.
 * Yeoman can be too opinionated at times but is easily convinced not to be.
 * Feel free to [learn more about Yeoman](http://yeoman.io/).

## License

unlicense © [Peter Huisman]()


[npm-image]: https://badge.fury.io/js/generator-p-element.svg
[npm-url]: https://npmjs.org/package/generator-p-element
[travis-image]: https://travis-ci.org//generator-p-element.svg?branch=master
[travis-url]: https://travis-ci.org//generator-p-element
[daviddm-image]: https://david-dm.org//generator-p-element.svg?theme=shields.io
[daviddm-url]: https://david-dm.org//generator-p-element
