'use strict';

const path = require('path');
const Generator = require('yeoman-generator');
const camel = require('to-camel-case');

module.exports = class extends Generator {
  prompting() {
    this.log('p-element generator 1.0.0');

    const prompts = [
      {
        type: 'input',
        name: 'name',
        message: 'Your element tag name (all lowercase and shoud have at least one -)',
        default: process
          .cwd()
          .split(path.sep)
          .pop()
          .replace(/\s+/g, '-')
          .toLowerCase()
      },
      {
        type: 'input',
        name: 'description',
        message: 'Element description',
        default: ''
      },
      {
        type: 'confirm',
        name: 'redux',
        message: 'Would you like to add redux?'
      },
      {
        type: 'input',
        name: 'developmentSetup',
        message: 'Do you want Docker or IIS for the local development setup? (docker/ iis)',
        default: 'iis'
      }
    ];

    return this.prompt(prompts).then(props => {
      this.props = props;
      let className = camel(props.name);
      this.props.className = className[0].toLocaleUpperCase() + className.substring(1);
      let arrayName = props.name.split('-');
      arrayName.shift();
      this.props.shortName = arrayName.join('-');
    });
  }

  writing() {
    this.fs.copyTpl(
      this.templatePath('demo/index.html'),
      this.destinationPath('demo/index.html'),
      {
        name: this.props.name,
        shortName: this.props.shortName,
        className: this.props.className,
        description: this.props.description
      }
    );

    this.fs.copyTpl(
      this.templatePath('demo/screen.css'),
      this.destinationPath('demo/screen.css'),
      {
        name: this.props.name
      }
    );

    this.fs.copy(
      this.templatePath('src/elementname.css'),
      this.destinationPath('src/' + this.props.shortName + '.css')
    );

    if (this.props.redux) {
      this.fs.copy(
        this.templatePath('src/actions/counter.ts'),
        this.destinationPath('src/actions/counter.ts')
      );
      this.fs.copy(
        this.templatePath('src/reducers/counter.ts'),
        this.destinationPath('src/reducers/counter.ts')
      );
      this.fs.copy(
        this.templatePath('src/helpers/lazy-reducers.ts'),
        this.destinationPath('src/helpers/lazy-reducers.ts')
      );
      this.fs.copy(
        this.templatePath('src/store.ts'),
        this.destinationPath('src/store.ts')
      );
    }

    this.fs.copyTpl(
      this.templatePath('src/elementname.css.d.ts'),
      this.destinationPath('src/' + this.props.shortName + '.css.d.ts'),
      {
        className: this.props.className
      }
    );

    this.fs.copyTpl(
      this.templatePath('src/elementname.spec.ts'),
      this.destinationPath('src/' + this.props.shortName + '.spec.ts'),
      {
        name: this.props.name,
        shortName: this.props.shortName,
        className: this.props.className,
        description: this.props.description
      }
    );

    this.fs.copyTpl(
      this.templatePath('src/elementname.tsx'),
      this.destinationPath('src/' + this.props.shortName + '.tsx'),
      {
        redux: this.props.redux,
        name: this.props.name,
        shortName: this.props.shortName,
        className: this.props.className,
        description: this.props.description
      }
    );

    this.fs.copy(
      this.templatePath('.editorconfig'),
      this.destinationPath('.editorconfig')
    );

    this.fs.copy(this.templatePath('gitignore'), this.destinationPath('.gitignore'));

    this.fs.copy(
      this.templatePath('karma.conf.js'),
      this.destinationPath('karma.conf.js')
    );

    this.fs.copyTpl(
      this.templatePath('package.json'),
      this.destinationPath('package.json'),
      {
        redux: this.props.redux,
        name: this.props.name,
        shortName: this.props.shortName,
        className: this.props.className,
        description: this.props.description
      }
    );

    this.fs.copyTpl(this.templatePath('readme.md'), this.destinationPath('readme.md'), {
      name: this.props.name,
      shortName: this.props.shortName,
      className: this.props.className,
      description: this.props.description
    });

    this.fs.copy(
      this.templatePath('tsconfig.json'),
      this.destinationPath('tsconfig.json')
    );

    this.fs.copy(this.templatePath('tslint.json'), this.destinationPath('tslint.json'));

    this.fs.copyTpl(
      this.templatePath('webpack.config.js'),
      this.destinationPath('webpack.config.js'),
      { name: this.props.name, shortName: this.props.shortName }
    );

    this.fs.copy(
      this.templatePath('webpack.config.karma.js'),
      this.destinationPath('webpack.config.karma.js')
    );

    if (this.props.developmentSetup.toLowerCase === "iis") {
      this.fs.copy(
        this.templatePath('setup-development.ps1'),
        this.destinationPath('setup-development.ps1')
      );
    } else if (this.props.developmentSetup.toLowerCase === "docker") {
      this.fs.copyTpl(
        this.templatePath('docker-compose.yml'),
        this.destinationPath('docker-compose.yml'),
        { name: this.props.name }
      );
    }
  }

  install() {
    this.installDependencies({ bower: false });
  }
};
