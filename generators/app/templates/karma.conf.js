module.exports = function (config) {

  const os = require("os");
  const webpackConfig = require('./webpack.config.karma.js');
  const path = require('path');
  const browsers = process.env.WEBDRIVERHOST ? ['ChromeSelenium'] : ['Chrome'];
  const webdriverConfig = {
    hostname: process.env.WEBDRIVERHOST,
    port: 80
  };

  config.set({
    hostname: os.hostname(),
    basePath: '',
    frameworks: ['jasmine'],
    files: [{
        pattern: './node_modules/p-elements-core/dist/p-elements-core.js',
        included: true,
        watched: false,
        served: true
      },
      './dist/**/*.*',
      './src/**/*.spec.ts'
    ],
    preprocessors: {
      'src/**/*.spec.ts': ['webpack']
    },
    mime: {
      'text/x-typescript': ['ts', 'tsx']
    },
    webpack: webpackConfig,
    reporters: ['dots','junit','coverage-istanbul'],
    coverageIstanbulReporter: {
      reports: ['cobertura', 'lcov'],
      dir: path.join(__dirname, 'coverage')
    },
    port: 9876,
    runnerPort: 9100,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    browsers: browsers,
    customLaunchers: {
      ChromeSelenium: {
          base: 'WebDriver',
          config: webdriverConfig,
          browserName: 'ChromeSelenium',
          flags: []
      }
    },
    singleRun: true
  });
};
