function New-DevSite() {
  $packageJSON = get-content .\package.json | ConvertFrom-Json;

  if ($packageJSON.urlPrefix) {
    $sitename = $packageJSON.urlPrefix;
  } else {
    throw "urlPrefix missing in package.json";
  }

  Write-Host "Checking if application pool $($sitename) has not yet been created."
  
  try {
    Get-WebAppPoolState -Name $sitename;
    Write-Host "  Application pool already exists, using this for the creation of the website" -ForegroundColor Green
  }
  catch {
    try {
      Write-Host "  Application pool has not yet been created, trying to create the application pool" -ForegroundColor Green
      New-WebAppPool -Name $sitename;
    }
    catch {
      Write-Host "  Application pool creation failed, using DefaultAppPool for the creation of the website`n" -ForegroundColor DarkYellow
    }
  }

  Write-Host "Checking if website $($sitename) has not yet been created."
  $siteExists = Get-WebSite -Name $sitename;

  if ($null -eq $siteExists) {
    Write-Host "  Website has not yet been created, creating website" -ForegroundColor Green

    $url = $sitename + ".dev";
    New-WebSite -Name $sitename -Port 443 -ApplicationPool $sitename -HostHeader $url -PhysicalPath "$(Get-Location)\demo" -Ssl -SslFlags 1 -Force;

    $rootCert = Set-RootCert
    $cert = Set-SiteCert -rootCert $rootCert -url $url

    $binding = Get-WebBinding -Name $sitename
    $binding.AddSslCertificate($cert.Thumbprint, "my")

    Add-ToHosts($url)
  } else {
    Write-Host "  The website $($sitename) already exists, skipping creation" -ForegroundColor DarkYellow
  }
}

function Add-ToHosts($url) {
  If ((Get-Content "$($env:windir)\system32\Drivers\etc\hosts" ) -notcontains "127.0.0.1 $url") {
    Add-Content -Encoding UTF8  "$($env:windir)\system32\Drivers\etc\hosts" "127.0.0.1 $url"
  }
}

function Set-SiteCert($rootCert, $url) {
  $cert = (Get-ChildItem cert:\LocalMachine\My\ | Where-Object { $_.subject -like "cn=$url" })
  if (!$cert) {
    $params = @{
      DnsName = $url
      Signer = $rootCert
      KeyLength = 2048
      KeyAlgorithm = 'RSA'
      HashAlgorithm = 'SHA256'
      KeyExportPolicy = 'Exportable'
      NotAfter = (Get-date).AddYears(2)
      CertStoreLocation = 'Cert:\LocalMachine\My'
    }
    $cert = New-SelfSignedCertificate @params
  }

  return $cert;
}

function Set-RootCert() {
  # Script om certicaten te maken voor gebruik op ontwikelmachine
  # Gaat uit van gebruik 'dev' domein, voeg deze toe aan je hosts file (C:\Windows\System32\drivers\etc)
  $rootCA = (Get-ChildItem cert:\LocalMachine\Root\ | Where-Object { $_.subject -like "cn=dev rootcert" })
  if (!$rootCA) {
    Write-Output "Aanmaken rootCA"
    $params = @{
      DnsName = "dev rootcert"
      KeyLength = 2048
      KeyAlgorithm = 'RSA'
      HashAlgorithm = 'SHA256'
      KeyExportPolicy = 'Exportable'
      NotAfter = (Get-Date).AddYears(2)
      CertStoreLocation = 'Cert:\LocalMachine\My'
      KeyUsage = 'CertSign','CRLSign' #fixes invalid cert error
    }
    $rootCA = New-SelfSignedCertificate @params

    # Add self-signed root to trusted root certificate store of current windows client
    # Extra step needed since self-signed cannot be directly shipped to trusted root CA store
    # if you want to silence the cert warnings on other systems you'll need to import the rootCA.crt on them too
    New-Item -ItemType Directory -Force -Path C:\certs
    Export-Certificate -Cert $rootCA -FilePath "C:\certs\rootCA.crt"
    Import-Certificate -CertStoreLocation 'Cert:\LocalMachine\Root' -FilePath "C:\certs\rootCA.crt"
  }

  return $rootCA;
}

New-DevSite