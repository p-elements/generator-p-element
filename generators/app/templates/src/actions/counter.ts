import { Action, ActionCreator } from "redux";

export const DECREMENT = "DECREMENT";
export const INCREMENT = "INCREMENT";

export interface CounterActionIncrement extends Action<"INCREMENT"> {}
export interface CounterActionDecrement extends Action<"DECREMENT"> {}

export type CounterAction = CounterActionIncrement | CounterActionDecrement;

export const increment: ActionCreator<CounterActionIncrement> = () => {
  return {
    type: INCREMENT,
  };
};

export const decrement: ActionCreator<CounterActionDecrement> = () => {
  return {
    type: DECREMENT,
  };
};
