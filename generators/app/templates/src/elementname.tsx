import "p-elements-core";

import * as css from "./<%= shortName %>.css";
<%
    if (redux) { %>
import { Store } from "redux";
import { decrement, increment } from "./actions/counter";
import { RootState, store } from "./store";
<%
    } %>
/**
 * <%= name %>
 *
 * @class <%= className %>
 * @extends {CustomElement}
 */
@CustomElementConfig({
  tagName: "<%= name %>",
})
export class <%= className %> extends CustomElement {

  /**
   * name
   *
   * @type {string}
   * @memberOf <%= className %>
   */
  @PropertyRenderOnSet
  public name: string = "";

  <%
    if (redux) { %>
  /**
   * state
   *
   * @type {RootState}
   * @memberOf <%= className %>
   */
  public state: RootState;

  /**
   * state
   *
   * @type {Store<any>}
   * @memberOf <%= className %>
   */
  public store: Store<any> = store;

  /**
   * storeUnsubscribe
   *
   * @private
   *
   * @memberOf <%= className %>
   */
  private storeUnsubscribe: () => void;

  /**
   * state changed
   *
   * @private
   *
   * @memberOf <%= className %>
   */
  private stateChanged = (newState: RootState) => {
    this.state = newState;
    this.renderNow();
  }
  <%
    } %>
  /**
   * render function
   *
   * @private
   *
   * @memberOf <%= className %>
   */
  private render = () => {
    return <div id="Sample" classes={
      {
        "sample": true,
        "sample__name-is-peter": this.name.toLowerCase() === "peter",
      }
    }>
      <%
        if (redux) { %>
      <a href="#" onclick={this.onMinClickHandler}>Min</a> <a href="#" onclick={this.onPlusClickHandler}>Plus</a>
      <p>
        Teller: {this.state && this.state.counter ? this.state.counter.value : "0"} <br />
        Clicks:  {this.state && this.state.counter ? this.state.counter.clicks : "0"}
      </p>
      <%
        } %>
      <p>Hello {this.name.length > 0 ? this.name : "World"}!!</p>
      <p><label for="Name">Naam</label> <input id="Name" oninput={this.onInput} value={this.name} /></p>
    </div>;
  }

  /**
   * on input event handler
   *
   * @private
   *
   * @memberOf <%= className %>
   */
  private onInput =  (event: Event) => {
    this.name = (event.target as HTMLInputElement).value;
    this.dispatchEvent(new CustomEvent("nameChange", {
      bubbles: false,
      cancelable: true,
      detail: this.name,
    }));
  }
  <%
    if (redux) { %>
  /**
   * on plus click event handler
   *
   * @private
   *
   * @memberOf <%= className %>
   */
  private onPlusClickHandler = (e: MouseEvent) => {
    e.preventDefault();
    this.store.dispatch(increment());
  }

  /**
   * on min click event handler
   *
   * @private
   *
   * @memberOf <%= className %>
   */
  private onMinClickHandler = (e: MouseEvent) => {
    e.preventDefault();
    this.store.dispatch(decrement());
  }
  <%
    } %>
  /**
   * init
   *
   * @private
   *
   * @memberOf <%= className %>
   */
  private init() {
    // init
  }

  /**
   * connectedCallback
   *
   * Custom element V1 connectedCallback()
   * Called every time the element is inserted into the DOM.
   * Useful for running setup code, such as fetching resources or rendering.
   * Generally, you should try to delay work until this time
   *
   * @private
   *
   * @memberOf <%= className %>
   */
  private connectedCallback() {
    const template = this.templateFromString(`
      <style>${css}</style>
      <div class="root"></div>`, true);
    this.shadowRoot.appendChild(template);

    const rootElement = this.shadowRoot.querySelector(".root");
    this.createProjector(rootElement, this.render);
    <%
    if (redux) { %>
    this.storeUnsubscribe = this.store.subscribe(() => this.stateChanged(this.store.getState()));
    <%
    } %>
  }

  /**
   * disconnectedCallback
   *
   * Custom element V1 disconnectedCallback()
   * Called every time the element is removed from the DOM.
   * Useful for running clean up code (removing event listeners, etc.).
   *
   * @private
   *
   * @memberOf <%= className %>
   */
  private disconnectedCallback() {
    //
    <%
    if (redux) { %>
    // Unsubscribe store
    if (this.storeUnsubscribe) {
      this.storeUnsubscribe();
    }
    <%
    } %>
  }

  /**
   * observedAttributes
   *
   * Custom element V1 observedAttributes property
   *
   * @readonly
   * @static
   * @type {[string]}
   * @memberOf <%= className %>
   */
  static get observedAttributes(): string[] {
    return ["name"];
  }

  /**
   * attributeChangedCallback
   *
   * Custom element V1  attributeChangedCallback(name, oldValue, newValue)
   * An attribute was added, removed, updated, or replaced. Also called for initial values when an element
   * is created by the parser, or upgraded. Note: only attributes listed in the observedAttributes property
   * will receive this callback.
   *
   * @private
   * @param {string} name
   * @param {string} oldValue
   * @param {string} newValue
   *
   * @memberOf <%= className %>
   */
  private attributeChangedCallback(name: string, oldValue: string, newValue: string) {
    switch (name) {
      case "name":
        this.name = newValue;
        break;
    }
  }

}
