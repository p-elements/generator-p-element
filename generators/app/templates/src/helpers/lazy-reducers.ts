export const lazyReducerEnhancer = (combineReducers) => {
  return (nextCreator) => {
    return (origReducer, preloadedState) => {
      let lazyReducers = {};
      const nextStore = nextCreator(origReducer, preloadedState);
      return {
        ...nextStore,
        addReducers(newReducers) {
          this.replaceReducer(combineReducers(lazyReducers = {
            ...lazyReducers,
            ...newReducers,
          }));
        },
      };
    };
  };
};
