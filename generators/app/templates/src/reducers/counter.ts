import { Reducer } from "redux";
import { DECREMENT,  INCREMENT } from "../actions/counter";
import { RootAction } from "../store";

export interface CounterState {
  clicks: number;
  value: number;
}

const defaultState = {
  clicks: 0,
  value: 0,
};

export const counter: Reducer<CounterState, RootAction> = ( state = defaultState, action) => {
  switch (action.type) {

    case DECREMENT:
      return {
        clicks: state.clicks + 1,
        value: state.value - 1,
      };

    case INCREMENT:
      return {
        clicks: state.clicks + 1,
        value: state.value + 1,
      };

    default:
      return {...state};

  }
};
