import { applyMiddleware, combineReducers, compose, createStore, Reducer, StoreEnhancer} from "redux";
import thunk, {ThunkMiddleware} from "redux-thunk";

import { CounterAction } from "./actions/counter";
import { lazyReducerEnhancer } from "./helpers/lazy-reducers";
import { counter, CounterState } from "./reducers/counter";

export type RootAction = CounterAction;

// Sets up a Chrome extension for time travel debugging.
// See https://github.com/zalmoxisus/redux-devtools-extension for more information.
const devCompose: <Ext0, Ext1, StateExt0, StateExt1>(
  f1: StoreEnhancer<Ext0, StateExt0>, f2: StoreEnhancer<Ext1, StateExt1>,
) => StoreEnhancer<Ext0 & Ext1, StateExt0 & StateExt1> =
  (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const store = createStore(
  (state) => state as Reducer<RootState, RootAction>,
  devCompose(
    lazyReducerEnhancer(combineReducers),
    applyMiddleware(thunk as ThunkMiddleware<RootState, RootAction>)),
);

// Initially reducers.
store.addReducers({
  counter,
});

export interface RootState {
  counter?: CounterState;
}
