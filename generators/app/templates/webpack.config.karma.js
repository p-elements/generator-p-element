'use strict';

const webpack = require('webpack');
const path = require('path');
const glob = require('glob');

module.exports = {
  mode: 'development',
  entry: glob.sync( './src/*.spec.ts').join("|").replace(/.spec.ts+/g,".spec").split("|"),
  module: {
    rules: [{
      test: /\.ts$/,
      include: [
        path.resolve(__dirname, 'src'),
      ],
      exclude: [
        /node_modules/
      ],
      loader: 'ts-loader',
    }]
  },
  resolve: {
    modules: [
      'node_modules',
    ],
    extensions: ['.ts', '.js'],
  },
  devtool: 'inline-source-map',
  stats: "verbose",
  plugins: [
    new webpack.SourceMapDevToolPlugin({
      filename: null,
      test: /\.(ts|js)($|\?)/i
    })
  ],
};
